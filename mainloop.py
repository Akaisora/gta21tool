#!/usr/bin/env python
# -*- coding: utf-8 -*-
# import cv2
import os,sys
import random,copy,time
from PIL import ImageGrab,Image
import numpy as np
from matplotlib import pyplot as plt
from skimage import io,feature,morphology,filters,color,measure
# import pytesseract
import win32api,win32con,win32gui
import queue
import traceback
import re
from wintools import *
from ocr_tool import Ocr_tool

def debugim(im):
    imm=np.asarray(im)
    io.imshow(imm)
    plt.show()


class Gta21tool(object):
    def __init__(self, stgfun):
        self.stx=0
        self.sty=0
        self.stgfun=stgfun
        self.action_region=(1233/1920, 962/1081, 1.0, 1.0)
        self.text_region=(29/1920, 40/1081, 481/1920, 201/1081)
        self.money_region=(1637/1920, 0.0, 1.0, 121/1081)
        self.card_region=(409/1920, 177/1081, 1497/1920, 665/1081)
        self.check_interval=5
        self.ocr_t=Ocr_tool()
        self.status={}

    def get_im(self):
        # im=io.imread('map_sample.png')
        # x=0;y=0
        pos1,pos2=get_window_pos()
        if pos1==(-1,-1):
            messagebox("Can't find game window!")
            raise
        im=ImageGrab.grab((pos1[0],pos1[1],pos2[0],pos2[1]))
        # im=np.asarray(im)
        x,y=pos1[1],pos1[0]
        #if len(im.shape)==3 and im.shape[2]==4:im=np.delete(im,3,2)
        stx=x
        sty=y
        return im

    def trans_to_screenpos(self, inpos):
        return (inpos[1]+sty,inpos[0]+stx)

    def exist(self, tag_lis, text_lis):
        res=[]
        for text in text_lis:
            for tag in tag_lis:
                if text in tag:
                    res.append((text, tag))
                    break

        return res

    def ocr_region(self, region):
        im=self.get_im()
        im=self.ocr_t.crop_image(im, region)
        tag_lis=self.ocr_t.ocr(image=im)
        return tag_lis      

    def wait_for(self, region, text_lis):
        print("wait for ",text_lis)
        while True:
            tag_lis=self.ocr_region(region)
            print(tag_lis)
            if self.exist(tag_lis, text_lis):
                res_lis=self.exist(tag_lis, text_lis)
                print("find ",res_lis)
                return res_lis
            time.sleep(self.check_interval)

    def check_exist(self, region, text_lis):
        print("check ",text_lis)

        tag_lis=self.ocr_region(region)
        print(tag_lis)
        res_lis=self.exist(tag_lis, text_lis)
        print("find ",res_lis)
        return res_lis     
    


        

    def run(self):
        print("BEGIN")
        while True:
            
            self.wait_for(self.action_region, ["下注"])
            key_input(["enter"])

            while True:
                self.status={}
                avaliable_actions=self.wait_for(self.action_region, ["双倍下注","停牌","拿牌"])
                self.status["actions"]=[x[0] for x in avaliable_actions]

                key_push(["page_down"])
                # key_push(["down_arrow"])
                res_text=self.wait_for(self.text_region, ["爆牌","您的牌为"])

                check_a=self.check_exist(self.card_region, ["A"])
                print(check_a)
                key_up(["page_down"])
                if "爆牌" in res_text[0][0]:
                    selfnum=-1
                else:
                    res=re.search(r"您的牌为(\d+)点", res_text[0][1]).group(1)
                    selfnum=int(res)

                key_push(["page_up"])
                res_text=self.wait_for(self.text_region, ["庄家的牌为"])
                key_up(["page_up"])
                res=re.search(r"庄家的牌为(\d+)点", res_text[0][1]).group(1)
                hostnum=int(res)
                
                self.status["hasA"]=bool(check_a)
                self.status["selfnum"]=selfnum
                self.status["hostnum"]=hostnum

                action=self.stgfun(self.status)
                print("ACTION", action)

                if action=="双倍下注":
                    key_input(["tab"])
                    break
                elif action=="停牌":
                    key_input(["spacebar"])
                    break
                elif action=="拿牌":
                    key_input(["enter"])
                else:
                    break
                
            # wait for result
            res=self.wait_for(self.text_region, ["您"])
            print("LOG RESULT ",res[0][1])
            money_res=self.ocr_region(self.money_region)
            print("MONEY ", money_res)

    






            









# MAIN PROCESS----------------------------


if __name__=="__main__":
    # for i in range(1000):
    #     print(i)
    #     im=get_im()
    #     im.save("./data/{}.png".format(i))
    #     #     time.sleep(5)

    def testfun():
        return "停牌"

    gta=Gta21tool(testfun)
    gta.run()

